/*
Copyright (C) 2020  michael mccune

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::env;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;


fn main() -> std::io::Result<()> {
    let path = env::current_dir()?;
    let hostport = "127.0.0.1:8000";
    let listener = TcpListener::bind(hostport).unwrap();
    println!("serving {} @ {}", path.display(), hostport);

    Ok(for stream in listener.incoming() {
        let stream = stream.unwrap();

        handle_connection(stream);
    })
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 512];

    stream.read(&mut buffer).unwrap();
    let request = String::from_utf8_lossy(&buffer[..]);
    let mut rlines = request.lines();
    let mut startline = rlines.next().unwrap().split_ascii_whitespace();
    let method = startline.next().unwrap();
    let target = startline.next().unwrap();

    let mut response = String::from("HTTP/1.1 200 OK\r\n\r\n");

    if method == "GET" {
        let body = get_file(target.to_string());
        response = match body {
            Ok(b) => {
                response.push_str(&b);
                String::from(response.as_str())
            },
            Err(_) => String::from("HTTP/1.1 404 NOT FOUND\r\n\r\n"),
        };
    }

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}

fn get_file(target: String) -> Result<String, Box<dyn Error>> {
    let filename = if target == "/" {
        String::from("./index.html")
    } else if target.starts_with("/") {
        let mut f = String::from(".");
        f.push_str(&target);
        f
    } else {
        // this shouldn't be possible, but just in case
        let mut f = String::from("./");
        f.push_str(&target);
        f
    };
    let file = File::open(filename)?;
    let mut reader = BufReader::new(file);
    let mut contents = String::new();
    reader.read_to_string(&mut contents)?;

    Ok(contents)
}
